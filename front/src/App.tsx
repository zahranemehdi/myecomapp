import "./App.css";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Jumbotron from "./components/Jumbotron";
import NavBar from "./components/NavBar";
import SubHeader from "./components/SubHeader";

function App() {
  return (
    <>
      <NavBar />
      <Header />
      <SubHeader />
      <Jumbotron />
      <Footer />
    </>
  );
}

export default App;
