import Product from "./Product";

interface Media extends Entity{
    name: string;
    description: string;
    content?: string;
    productId?: string;
    product: Product;
}

export default Media;