import Category from "./Category";
import Comment from "./Comment";
import Media from "./Media";

interface Product extends Entity {
    brandLabel?: string;
    title: string;
    description?: string;
    technicalDescription?: string;
    discount: number;
    FinalPrice?: number;
    inPromotion: boolean;
    countInStock: number;
    notes?: number;
    discountPrice?: number;
    sizes?: string[];
    comments?: Comment[];
    categories: Category[];
    medias: Media[];

  }
export default Product;