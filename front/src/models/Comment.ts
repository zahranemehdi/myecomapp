interface Comment extends Entity{
    userName?: string;
    content?: string;
    createdLastTime?: string;
}

export default Comment;