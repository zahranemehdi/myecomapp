interface Category extends Entity{
    name: string;
    childCategoriesIds: string[];
}

export default Category;