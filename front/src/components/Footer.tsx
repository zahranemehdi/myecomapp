import React from "react";

const Footer = () => {
  return (
    <>
      <footer className="mt-auto text-center py-4 border-top">
        <p>
          Made in <i className="bi bi-moon-stars"></i>, by{" "}
          <i className="bi bi-cup-hot"></i> and
          <a href="https://gitlab.com/zahranemehdi" className="link-dark">
            @zahranemehdi
          </a>
          .
        </p>
      </footer>
    </>
  );
};

export default Footer;
