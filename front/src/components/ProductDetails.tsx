import NavBar from "./NavBar";
import Product from "../models/Product";
import { useParams } from "react-router-dom";
import Footer from "./Footer";

const ProductDetails = () => {
  const params = useParams();
  const productId = params.productId;

  // Search in api for product by Id
  const productSample: Product = {
    id: productId ?? "",
    brandLabel: "Orianz",
    title: "Men's slim fit t-shirt",
    lastPrice: 45.95876,
    description:
      "Shop from a wide range of t-shirt from orianz. Pefect for your everyday use, you could pair it with a stylish pair of jeans or trousers complete the look.",
    discount: 25,
    technicalDescription:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum atque, nulla vel eveniet cupiditate nobis. Laboriosam aspernatur recusandae, quo provident, voluptas doloremque adipisci vitae aperiam soluta nobis nostrum voluptatum facere nulla mollitia ut harum? Inventore eius quo quae, officia itaque repellendus debitis laudantium aliquam a tempora voluptas delectus, facilis ipsa eos placeat atque libero nulla nostrum minus expedita velit cupiditate. Molestiae, eos necessitatibus. Minus inventore illum neque ipsa aliquam voluptates. Nihil reprehenderit repellat enim sit repudiandae aut suscipit necessitatibus corrupti molestiae deserunt? Odio sunt non culpa cupiditate saepe quibusdam, dolorem laudantium, placeat voluptate ut reprehenderit nisi a suscipit ad dignissimos exercitationem? Dolorem, molestiae nesciunt? Blanditiis molestias qui omnis quidem exercitationem eveniet illo dolorem, facere, esse velit sed, pariatur corrupti tempore. A commodi magnam distinctio dolore amet doloribus nulla iste tempora aspernatur hic perspiciatis minus, quibusdam deleniti harum repellendus possimus ullam esse, aliquid quidem placeat incidunt dolorem labore. Hic, tempora laudantium.technicalDescription",
    medias: [
      "https://i.imgur.com/Dhebu4F.jpg",
      "https://i.imgur.com/Rx7uKd0.jpg",
      "https://i.imgur.com/Dhebu4F.jpg",
    ],
    categories: ["baskets", "shoes", "sports"],
    sizes: ["small", "medium", "large"],
    notes: 4.5,
    comments: [
      {
        id: "",
        userName: "nana",
        content: "Great product !!!",
        createdLastTime: "now",
      },
      {
        id: "",
        userName: "user doe",
        content: "Bad product !!!!",
        createdLastTime: "3d",
      },
      {
        id: "",
        userName: "jon wick",
        content: "Standard product, price is overrated !!",
        createdLastTime: "1w",
      },
    ],
  };

  const handleChange = () => {
    console.log("Choosed");
  };
  return (
    <>
      <NavBar />
      <p></p>
      <div className="container my-5">
        <i className="bi bi-arrow-left"></i>
        <span className="mx-2">
          <a href="/">Back</a>
        </span>
        <div className="row d-flex justify-content-center">
          <div className="col-md-10">
            <div className="card">
              <div className="row">
                <div className="col-md-6">
                  <div className="images p-3">
                    <div className="text-center p-4">
                      {" "}
                      <img
                        id="main-image"
                        src={productSample.medias[0]}
                        width="250"
                      />{" "}
                    </div>
                    <div className="thumbnail text-center">
                      <img src={productSample.medias[1]} width="70" />{" "}
                      <img src={productSample.medias[2]} width="70" />{" "}
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="product p-4">
                    <div className="d-flex justify-content-between align-items-center">
                      <i className="fa fa-shopping-cart text-muted"></i>
                    </div>
                    <div className="mt-4 mb-3">
                      <span className="text-uppercase text-muted brand">
                        {productSample.brandLabel}
                      </span>
                      <h5 className="text-uppercase">{productSample.title}</h5>
                      <div className="price d-flex flex-row align-items-center">
                        {" "}
                        <span className="act-price">
                          ${productSample.lastPrice?.toFixed(2)}
                        </span>
                        <div className="mx-2">
                          <span>{productSample.discount}% OFF</span>{" "}
                        </div>
                      </div>
                    </div>
                    <p className="about">{productSample.description}</p>
                    <div className="mt-5">
                      <h6 className="text-uppercase">Sizes</h6>{" "}
                      <div
                        className="btn-group"
                        role="group"
                        aria-label="Basic radio toggle button group"
                      >
                        {productSample.sizes?.map((size, index) => (
                          <div key={index}>
                            <input
                              type="radio"
                              className="btn-check"
                              name="btnRadio"
                              onChange={handleChange}
                            ></input>
                            <label className="btn btn-outline-primary">
                              {size}
                            </label>
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="cart mt-4 align-items-center">
                      {" "}
                      <button className="btn btn-dark text-uppercase mr-2 px-4">
                        Add to cart
                      </button>{" "}
                      <i className="fa fa-heart text-muted"></i>{" "}
                      <i className="fa fa-share-alt text-muted"></i>{" "}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-5">
              <div className="col-md ">
                <h3 className="fs fs-4 text-center">Description : </h3>
                <p className="mx-2">{productSample.technicalDescription}</p>
                <p></p>
              </div>
            </div>
            <div className="row mt-5">
              <div className="col-md">
                <h3 className="fs fs-4 text-center mb-2">
                  What our customers says about it :{" "}
                </h3>
                <div className="list-group">
                  {productSample.comments?.map((comment, index) => (
                    <div
                      key={index}
                      className="list-group-item list-group-item-action d-flex gap-3 py-3"
                      aria-current="true"
                    >
                      <img
                        src="https://github.com/twbs.png"
                        alt="twbs"
                        width="32"
                        height="32"
                        className="rounded-circle flex-shrink-0"
                      />
                      <div className="d-flex gap-2 w-100 justify-content-between">
                        <div>
                          <h6 className="mb-0">{comment.userName}</h6>
                          <p className="mb-0 opacity-75">{comment.content}</p>
                        </div>
                        <small className="opacity-50 text-nowrap">
                          {comment.createdLastTime}
                        </small>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default ProductDetails;
