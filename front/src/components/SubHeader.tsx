import { useEffect, useState } from "react";
import Category from "../models/Category";
import categoriesHttpService from "../services/categories-service";
import { CanceledError } from "../services/api-client";
const SubHeader = () => {
  // set categories state
  const [categories, setCategories] = useState<Category[]>([]);
  useEffect(() => {
    const { request, cancel } = categoriesHttpService("/categories").getAll();
    request
      .then((res) => {
        const _result = res.data.filter((x) => x.childCategoriesIds != null);
        setCategories(_result);
      })
      .catch((error) => {
        if (error instanceof CanceledError) return;
        setCategories([]);
      });
    return () => cancel();
  }, []);
  console.log(categories);
  return (
    <>
      <div className="text-center my-5">
        <h1 className="display-5">Explore more</h1>
      </div>
      <div
        className="hstack gap-3 border-top border-bottom py-3"
        style={{ justifyContent: "center" }}
      >
        {categories.map((category) => (
          <div className="p-2" key={category.id}>
            <a
              href="#"
              className="link-dark"
              style={{ textDecoration: "none" }}
            >
              {category.name}
            </a>
          </div>
        ))}
      </div>
    </>
  );
};

export default SubHeader;
