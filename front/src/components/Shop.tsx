import NavBar from "./NavBar";
import Footer from "./Footer";
import Product from "../models/Product";

const Shop = () => {
  const productSample: Product = {
    id: "653c17a1d3370416c9699b68",
    title: "Product title",
    discount: 25,
    medias: [``],
    categories: ["baskets", "shoes", "sports"],
  };
  let productsArray: Product[] = [];
  for (let i = 0; i < 12; i++) {
    productsArray.push(productSample);
  }
  return (
    <>
      <NavBar />
      <div className="text-center my-3">
        <h1 className="display-5">Shop and discover many brands</h1>
      </div>

      <div className="container-fluid my-5">
        <div className="row">
          <div className="col-lg-3">
            <p className="fs-3">Brands to shop : </p>
            <ul className="list-group">
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <a
                  href="#"
                  className="link-dark"
                  style={{ textDecoration: "none" }}
                >
                  A second list item
                </a>
                <span className="badge bg-dark rounded-pill">14</span>
              </li>
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <a
                  href="#"
                  className="link-dark"
                  style={{ textDecoration: "none" }}
                >
                  A second list item
                </a>
                <span className="badge bg-dark rounded-pill">2</span>
              </li>
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <a
                  href="#"
                  className="link-dark"
                  style={{ textDecoration: "none" }}
                >
                  A second list item
                </a>
                <span className="badge bg-dark rounded-pill">2</span>
              </li>
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <a
                  href="#"
                  className="link-dark"
                  style={{ textDecoration: "none" }}
                >
                  A second list item
                </a>
                <span className="badge bg-dark rounded-pill">2</span>
              </li>
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <a
                  href="#"
                  className="link-dark"
                  style={{ textDecoration: "none" }}
                >
                  A second list item
                </a>
                <span className="badge bg-dark rounded-pill">2</span>
              </li>
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <a
                  href="#"
                  className="link-dark"
                  style={{ textDecoration: "none" }}
                >
                  A third list item
                </a>
                <span className="badge bg-dark rounded-pill">1</span>
              </li>
            </ul>
          </div>
          <div className="col-lg-9">
            <div className="row gap-3" style={{ margin: "0px" }}>
              {productsArray.map((product, index) => (
                <div
                  key={index}
                  className="col-lg-4 card text-center"
                  style={{ flex: "0 0 auto", width: "32.33333333%" }}
                >
                  <div className="card-body">
                    <h4 className="card-title">{product.title}</h4>
                    <h6 className="card-subtitle mb-2 text-muted">
                      -{product.discount}% discount
                    </h6>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="d-block user-select-none"
                      width="100%"
                      height="200"
                      aria-label="Placeholder: Image cap"
                      focusable="false"
                      role="img"
                      preserveAspectRatio="xMidYMid slice"
                      viewBox="0 0 318 180"
                      style={{ fontSize: "1.125rem", textAnchor: "middle" }}
                    >
                      <rect width="100%" height="100%" fill="#868e96"></rect>
                      <text x="50%" y="50%" fill="#dee2e6" dy=".3em">
                        Image cap
                      </text>
                    </svg>
                    <p className="card-text"></p>
                    <button className="btn btn-dark" type="button">
                      <i className="bi bi-cart"></i> Add to cart
                    </button>
                  </div>
                  <div className="card-footer text-body-secondary">
                    {product.categories.map((category) => (
                      <span
                        key={category}
                        className="badge rounded-pill bg-dark mx-1 "
                      >
                        <a
                          href="#"
                          className="text-white"
                          style={{ textDecoration: "none" }}
                        >
                          {category}
                        </a>
                      </span>
                    ))}
                  </div>
                </div>
              ))}
            </div>
            <nav className="my-4 d-flex justify-content-center">
              <ul className="pagination">
                <li className="page-item">
                  <a className="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    4
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    5
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    6
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    7
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    8
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    9
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    10
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default Shop;
