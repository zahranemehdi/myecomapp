import React from "react";

const Jumbotron = () => {
  return (
    <>
      <div className="container my-5">
        <div className="p-5 text-center bg-body-tertiary rounded-3">
          <h1 className="text-body-emphasis">Lorem ipsum</h1>
          <p className="lead">
            Lorem, <code>ipsum</code> dolor sit amet consectetur adipisicing
            elit. Laboriosam odit necessitatibus, <code>repudiandae</code>{" "}
            explicabo quibusdam cumque at totam unde tempore, <code>nobis</code>{" "}
            porro ducimus iste commodi blanditiis.
          </p>
        </div>
      </div>
    </>
  );
};

export default Jumbotron;
