const Header = () => {
  return (
    <>
      <div className="text-center my-3">
        <h1 className="display-5">Discover our new brands and more</h1>
      </div>
      <div className="container text-center my-5">
        <div className="row">
          <div className="col-lg-4">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Card title</h4>
                <h6 className="card-subtitle mb-2 text-muted">-20% discount</h6>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="d-block user-select-none"
                  width="100%"
                  height="200"
                  aria-label="Placeholder: Image cap"
                  focusable="false"
                  role="img"
                  preserveAspectRatio="xMidYMid slice"
                  viewBox="0 0 318 180"
                  style={{ fontSize: "1.125rem", textAnchor: "middle" }}
                >
                  <rect width="100%" height="100%" fill="#868e96"></rect>
                  <text x="50%" y="50%" fill="#dee2e6" dy=".3em">
                    Image cap
                  </text>
                </svg>
                <p className="card-text"></p>
                <button className="btn btn-dark" type="button">
                  <i className="bi bi-cart"></i> Add to cart
                </button>
                <a href="#" className="card-link mx-2">
                  Discover more
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Card title</h4>
                <h6 className="card-subtitle mb-2 text-muted">-20% discount</h6>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="d-block user-select-none"
                  width="100%"
                  height="200"
                  aria-label="Placeholder: Image cap"
                  focusable="false"
                  role="img"
                  preserveAspectRatio="xMidYMid slice"
                  viewBox="0 0 318 180"
                  style={{ fontSize: "1.125rem", textAnchor: "middle" }}
                >
                  <rect width="100%" height="100%" fill="#868e96"></rect>
                  <text x="50%" y="50%" fill="#dee2e6" dy=".3em">
                    Image cap
                  </text>
                </svg>
                <p className="card-text"></p>
                <button className="btn btn-dark" type="button">
                  <i className="bi bi-cart"></i> Add to cart
                </button>
                <a href="#" className="card-link mx-2">
                  Discover more
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Card title</h4>
                <h6 className="card-subtitle mb-2 text-muted">-20% discount</h6>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="d-block user-select-none"
                  width="100%"
                  height="200"
                  aria-label="Placeholder: Image cap"
                  focusable="false"
                  role="img"
                  preserveAspectRatio="xMidYMid slice"
                  viewBox="0 0 318 180"
                  style={{ fontSize: "1.125rem", textAnchor: "middle" }}
                >
                  <rect width="100%" height="100%" fill="#868e96"></rect>
                  <text x="50%" y="50%" fill="#dee2e6" dy=".3em">
                    Image cap
                  </text>
                </svg>
                <p className="card-text"></p>
                <button className="btn btn-dark" type="button">
                  <i className="bi bi-cart"></i> Add to cart
                </button>
                <a href="#" className="card-link mx-2">
                  Discover more
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container text-center">
        <button
          type="button"
          className="btn btn-outline-secondary rounded-pill"
        >
          Discover more
        </button>
      </div>
    </>
  );
};

export default Header;
