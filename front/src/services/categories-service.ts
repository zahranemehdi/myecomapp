import { AxiosResponse } from "axios";
import Category from "../models/Category";
import apiClient from "./api-client";

class CategoriesHttpService{

    endPoint: string;

    constructor(endPoint: string){
        this.endPoint = endPoint;
    }

    getAll(loadSubObjects: boolean = false){
        const controller = new AbortController();
        let request: Promise<AxiosResponse<Category[], any>>;
        if (loadSubObjects){
            request = apiClient.get<Category[]>(this.endPoint + "?loadSubObjects=true", { signal: controller.signal});
        }
        else{
            request = apiClient.get<Category[]>(this.endPoint, { signal: controller.signal});
        }

        return { request, cancel: () => controller.abort()};
    }
}
const categoriesHttpService = (endPoint: string) => new CategoriesHttpService(endPoint);
export default categoriesHttpService;