import { useEffect, useState } from "react";

import { CanceledError } from "../services/api-client";
import Category from "../models/Category";
import categoriesHttpService from "../services/categories-service";

const useUsers = () => {
    const httpRoute: string = "/categories";
    const [categories, setCategories] = useState<Category[]>([]);
    const [error, setError] = useState("");
    const [isLoading, setLoading] = useState(false);
  
    useEffect(() => {
      setLoading(true);
      const { request, cancel } = categoriesHttpService(httpRoute).getAll();
      request.then((res) => {
          setCategories(res.data);
          setLoading(false);
          setError("");
        }).catch((error) => {
          if (error instanceof CanceledError) return;
          setError(error.message);
          setLoading(false);
          setCategories([]);
        });
      return () => cancel();
    }, []);
    return { categories, error, isLoading, setCategories, setError, setLoading};
}
export default useUsers;