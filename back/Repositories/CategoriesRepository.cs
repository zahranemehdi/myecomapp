﻿using back.Models;
using back.Utils;
using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back.Repositories
{
    public class CategoriesRepository
    {
        private readonly SqlConnectionSettings _sqlConnectionSettings;

        public CategoriesRepository(SqlConnectionSettings sqlConnectionSettings)
        {
            _sqlConnectionSettings = sqlConnectionSettings;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            try
            {
                var result = new List<Category>();
                using(var connection = _sqlConnectionSettings.SqlConnection)
                {
                    await connection.OpenAsync();           

                    string sqlQuery = "select * from [dbo].[categories]";

                    result = connection.Query<Category>(sqlQuery).ToList();
                }
                return result;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


        public async Task<Category> GetAsync(int id)
        {
            if (id == 0)
                throw new Exception("Cannot get category of id == 0.");

            try
            {
                Category result = null;
                using (var connection = _sqlConnectionSettings.SqlConnection)
                {
                    string query = $"select * from [dbo].[categories] where [Id] = {id}";
                    result = await connection.QueryFirstOrDefaultAsync<Category>(query);
                    
                    // Set subobjects if exists 
                    if (result == null)
                    {
                        throw new Exception("No recorrds with the given id.");
                    }

                    string subQuery = $"select * from [dbo].[categories] where ParentCategoryId = ${result.Id}";

                    var resultSubQuery = await connection.QueryAsync<Category>(subQuery);

                    if (resultSubQuery != null && resultSubQuery.Count() > 0)
                    {
                        result.ChildCategories = new List<Category>();
                        foreach (var subCategory in resultSubQuery)
                        {
                            result.ChildCategories.Add(subCategory);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CreateAsync(Category category)
        {
            if (category == null)
                throw new Exception("Cannot insert null category object.");

            bool result = false;
            try
            {
                using (var connection = _sqlConnectionSettings.SqlConnection)
                {
                    await connection.OpenAsync();


                    string sqlQuery = @"insert into [dbo].Categories (Name, Created_at) 
                                        values(@categoryName, GETDATE())";

                    var rowsAffected = await connection.ExecuteAsync(sqlQuery, new { categoryName = category.Name });
                    result = rowsAffected > 0;
                }
            }
            catch
            {
                result = false;
            }
            return result;


        }
    }
}
