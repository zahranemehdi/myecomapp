﻿using System.Collections.Generic;

namespace back.Models.Api
{
    public class Response
    {
        public object? Data { get; set; }

        public bool Success { get; set; } = false;
        public List<string> Errors { get; set; } = new();
        public List<string> Warnings { get; set; } = new();
        public List<string> Informations { get; set; } = new();
    }
}
