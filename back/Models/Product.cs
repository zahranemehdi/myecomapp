using System.Collections.Generic;

namespace back.Models
{
    public class Product : Entity
    {
        public string BrandLabel { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public string TechnicalDescription { get; set; } = string.Empty;

        public decimal Notes { get; set; }

        public decimal CatalogPrice { get; set; }

        public decimal DiscountPrice { get; set; }

        public bool InPromotion { get; set; }

        public int CountInStock { get; set; } = 0;

        public List<string> CategoriesIds { get; set; } = new();
        public List<Category> Categories { get; set; } = new();

        public List<string> MediasIds { get; set; } = new();
        public List<Media> Medias { get; set; } = new();

    }
}