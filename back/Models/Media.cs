namespace back.Models
{
    public class Media : Entity
    {
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string? Content { get; set; }

        public string? ProductId { get; set; }

        public Product? Product { get; set; }


    }
}