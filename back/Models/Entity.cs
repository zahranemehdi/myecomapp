using System;

namespace back.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }

        public DateTime? Created_At { get; set; }

        public DateTime? Update_At { get; set; }
    }
}