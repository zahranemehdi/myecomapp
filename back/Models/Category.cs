using System.Collections.Generic;

namespace back.Models
{
    public class Category : Entity
    {
        public string Name { get; set; } = string.Empty;

        public int? ParentCategoryId { get; set; }

        public List<Category>? ChildCategories { get; set; }

        public List<Product> Products { get; } = new();

    }
}