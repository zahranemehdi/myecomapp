﻿USE [MyEcomAppDB]
GO

BEGIN TRY 
if (select count(*) from [dbo].Categories) > 1 
BEGIN
	delete from [dbo].Categories;
END

DBCC CHECKIDENT (Categories, RESEED, 0);

insert into [dbo].Categories (Name, Created_at)
values ('Men', GETDATE()),
		('Women', GETDATE()),
		('Collections', GETDATE()),
		('Promotions', GETDATE()),
		('Clothes', GETDATE()),
		('Shoes', GETDATE()),
		('Accessories', GETDATE());

insert into [dbo].Categories (Name, ParentCategoryId, Created_at)
values ('Clothes', (select id from [dbo].Categories where LOWER(name) = 'men'), GETDATE()),
		('Shoes', (select id from [dbo].Categories where LOWER(name) = 'men'), GETDATE()),
		('Accessories', (select id from [dbo].Categories where LOWER(name) = 'men'), GETDATE()),
		('Clothes', (select id from [dbo].Categories where LOWER(name) = 'women'), GETDATE()),
		('Shoes', (select id from [dbo].Categories where LOWER(name) = 'women'), GETDATE()),
		('Accessories', (select id from [dbo].Categories where LOWER(name) = 'women'), GETDATE()),
		('Clothes', (select id from [dbo].Categories where LOWER(name) = 'collections'), GETDATE()),
		('Shoes', (select id from [dbo].Categories where LOWER(name) = 'collections'), GETDATE()),
		('Accessories', (select id from [dbo].Categories where LOWER(name) = 'collections'), GETDATE()),
		('Clothes', (select id from [dbo].Categories where LOWER(name) = 'accessories'), GETDATE()),
		('Shoes', (select id from [dbo].Categories where LOWER(name) = 'accessories'), GETDATE()),
		('Accessories', (select id from [dbo].Categories where LOWER(name) = 'accessories'), GETDATE());

END TRY
BEGIN CATCH
	Print 'In catch block..';
	THROW;
END CATCH;
GO