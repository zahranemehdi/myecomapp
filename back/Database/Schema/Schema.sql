﻿USE [MyEcomAppDB]
GO
/****** Object:  Table [dbo].[Medias]    Script Date: 01/11/2023 13:03:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'dbo.Categories', N'U') IS NOT NULL
BEGIN
    DROP table [dbo].Categories;
END

CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1)  NOT NULL ,
	[Name] [nvarchar](50) NOT NULL,
	[ParentCategoryId] [int] NULL,
	[Created_at] DATETIME NULL,
	[Update_at] DATETIME NULL
	PRIMARY KEY (ID)
);

Print 'Categories table created.';

IF OBJECT_ID(N'dbo.Products', N'U') IS NOT NULL
BEGIN
    DROP table [dbo].Products;
END

CREATE TABLE [dbo].Products(
	[Id] [int] IDENTITY(1,1)  NOT NULL ,
	[BrandLabel] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](2500) NULL,
	[TechnicalDescription] [nvarchar](2500) NULL,
	[Notes] decimal(10,2) NULL,
	[CatalogPrice] decimal(10,2) NULL,
	[Discount] decimal(10,2) NULL,
	[InPromotion] bit NULL,
	[CountInStock] int default 0,
	[Created_at] DATETIME NULL,
	[Update_at] DATETIME NULL
	PRIMARY KEY (Id)
);

Print 'Products table created.';

IF OBJECT_ID(N'dbo.Medias', N'U') IS NOT NULL
BEGIN
    DROP table [dbo].Medias;
END

CREATE TABLE [dbo].Medias(
	[Id] [int] IDENTITY(1,1)  NOT NULL ,
	[Name] [nvarchar](255) NOT NULL,
	[AltDescription] [nvarchar](255) NULL,
	[ContentBase64String] nvarchar(max) null,
	[ProductId] int Null,
	[Created_at] DATETIME NULL,
	[Update_at] DATETIME NULL
	PRIMARY KEY (ID)
);

Print 'Medias table created.';

Print 'Setting foreign keys for categories table.';
ALTER TABLE [dbo].[Categories]
	ADD FOREIGN KEY (ParentCategoryId) REFERENCES Categories(Id); 

Print 'Setting many to many relation between categories and products.';
IF OBJECT_ID(N'dbo.Categories_Products', N'U') IS NOT NULL
BEGIN
    DROP table [dbo].Categories_Products;
END

CREATE TABLE [dbo].Categories_Products(
	[CategoryId] int NOT NULL foreign key references [dbo].Categories(Id),
	[ProductId] int NOT NULL foreign key references [dbo].Products(Id)

	constraint PK_Categories_Products primary key (CategoryId, ProductId)
);

Print 'Setting relation between products and medias.';
ALTER TABLE [dbo].[Medias]
	ADD FOREIGN KEY (ProductId) REFERENCES Products(Id); 

Print 'End';

GO


