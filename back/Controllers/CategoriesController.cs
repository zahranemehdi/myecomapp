using System.Threading.Tasks;
using back.Models.Api;
using back.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace back.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesService _categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {
            _categoriesService = categoriesService;
        }

        #region Preflight requests

        /// <summary>
        /// Preflight request for SPA
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpOptions("{id:int}")]
        public IActionResult PreflightRoute(int id)
        {
            return NoContent();
        }

        /// <summary>
        /// Preflight request for SPA
        /// </summary>
        /// <returns></returns>
        [HttpOptions]
        public IActionResult PreflightRoute()
        {
            return NoContent();
        }

        #endregion


        /// <summary>
        /// Get all Categories
        /// </summary>
        /// <param name="loadSubCategories"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<Response>> Get([FromQuery] bool loadSubCategories = false)
        {
            var result = await _categoriesService.GetAllAsync();
            return Ok(result);
        }


        /// <summary>
        /// Get category by id.
        /// </summary>
        /// <param name="id">Id category.</param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<Response>> Get([FromRoute] int id)
        {
            var result = await _categoriesService.GetAsync(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<Response>> Create([FromBody] CategoryCreateRequest categoryCreateRequest)
        {
            var result = await _categoriesService.CreateAsync(categoryCreateRequest);
            return Ok(result);
        }
    }
}