﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace back.Utils
{
    public class SqlConnectionSettings
    {
        private SqlConnection _sqlConnection;

        private IConfiguration _configuration;

        public SqlConnectionSettings(IConfiguration configuration) => _configuration = configuration;

        public SqlConnection SqlConnection { 
            get {
                string connectionString = _configuration.GetSection("DbConnection").Value;

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

                _sqlConnection = new SqlConnection(builder.ConnectionString);

                return _sqlConnection; 
            } 
        }
    }
}
