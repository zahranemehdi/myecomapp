using back.Models.Api;
using back.Repositories;
using back.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace back.Services
{
    public class CategoriesService : ICategoriesService
    {
        private readonly CategoriesRepository _categoriesRepository;

        public CategoriesService(CategoriesRepository categoriesRepository)
        {
            _categoriesRepository = categoriesRepository;
        }

        public async Task<Response> GetAllAsync()
        {
            Response result = new Response();
            try
            {
                var resultRepo = await _categoriesRepository.GetAllAsync();
                result.Success = true;
                result.Data = resultRepo;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Errors.Add(ex.Message);
            }
            return result;
        }

        public async Task<Response> GetAsync(int id)
        {
            Response result = new Response();
            try
            {
                var resultRepo = await _categoriesRepository.GetAsync(id);
                result.Success = true;
                result.Data = resultRepo;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Errors.Add(ex.Message);
            }
            return result;
        }

        public async Task<Response> CreateAsync(CategoryCreateRequest categoryCreateRequest)
        {
            Response result = new Response();

            if (categoryCreateRequest == null)
            {
                result.Errors.Add("Cannot create null objects.");
                return result;
            }

            if (string.IsNullOrEmpty(categoryCreateRequest.CategoryName))
            {
                result.Errors.Add("Cannot create category with empty name.");
                return result;
            }

            try
            {
                categoryCreateRequest.CategoryName = categoryCreateRequest.CategoryName.TrimEnd().TrimStart();
                var resultRepo = await _categoriesRepository.CreateAsync(new Models.Category { Name = categoryCreateRequest.CategoryName});
                result.Success = true;
                result.Data = resultRepo;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Errors.Add(ex.Message);
            }
            return result;
        }
    }
}