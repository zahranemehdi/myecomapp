﻿using back.Models.Api;
using System.Threading.Tasks;

namespace back.Services.Contracts
{
    public interface ICategoriesService
    {
        Task<Response> GetAllAsync();

        Task<Response> GetAsync(int id);

        Task<Response> CreateAsync(CategoryCreateRequest categoryCreateRequest);
    }
}
